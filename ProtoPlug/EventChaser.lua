--
---  EventChaser
--
-- This script will forward CC, PitchBend and ChannelPressure aftertouch
-- messages to all channels between 1-127 where notes are currently 
-- active (and after NoteOff for a short continuation period).
--
-- @script      com.example.script
-- @type        DSP
-- @author      Steve Schow
-- 
-- Version 3.00
-- 
-- LuaProtoPlug version (note requires my own custom API additions/changes
-- 
------------------------------------------------------------------------

require "include/protoplug"

-----------------------------------------------------
-- Some temporary global variables until I figure out
-- how to use parameters and UI stuff properly
-----------------------------------------------------
gFollowCC = true
gFollowPB = true
gFollowAT = true
gFollowPP = true
-- set to 100ms as ms / 1000 - sampleRate
gContinuation=0
gSampleRate=0

-------------------------------------------------------------
-- Some globals that will be used throughout script to avoid
-- having to pass around a million parameters
-------------------------------------------------------------
gOutput = {}
STOPPED=true

-- setup global tables for src and dest status, dest has 16 channels
gSrcStatus = {}
gDestStatus = {}

-----------------------------------
-- process callback
-----------------------------------
function plugin.processBlock(samples, smax, midiBuf)

    -- first check playing status, if recently started, then
    -- reset status

    local transport = plugin.getCurrentPosition()

    if(transport.isPlaying) then
        if(STOPPED) then
            resetStatus()
            STOPPED = false
        end
    else
        STOPPED=true
    end

    -- midi output table
    gOutput = {}

    -- loop through input buffer, handle each midi msg
    -- TODO, only one buffer means we can't refer to any 
    --       underlying midi objets by ref, and this is an 
    --       area for performance improvement I think.
    --       would be nice to use actual MidiBuffer directly
    --       intead of using a table and then do a swap
    --       at the end perhaps or something.
    --       Plus if we are going to do all this copying
    --       then we should have legit event object classes

	for msg in midiBuf:eachEvent() do

         -- pass PC through as-is
        if (msg:isProgram()) then
            -- copy by value midi event to output table
            -- TODO, possible to use a new MIdiBUffer directly?
            scheduleMidi(msg)

        -- NoteOn
        elseif (msg:isNoteOn()) then
            handleNoteOn(msg)

         -- NoteOff
        elseif (msg:isNoteOff()) then
            handleNoteOff(msg)

         -- CC, AT, PP and PB Expression msgs
        else
            handleExpression(msg)
        end
    end

	-- fill midi buffer with prepared notes
	midiBuf:clear()
	if #gOutput>0 then
		for _,e in ipairs(gOutput) do
			midiBuf:addEvent(e)
		end
	end

end


-------------------------------------------------------------
-- resetStatus method called by Reset callback 
-- resets all running status
-------------------------------------------------------------
function resetStatus()

    gSrcStatus = {}
    gSrcStatus.cc = {}
    gSrcStatus.pp = {}
    gSrcStatus.notes = 0
    gSrcStatus.lastSample = 0
    gSrcStatus.at = nil
    gSrcStatus.pb = nil

    gDestStatus = {}
    for chan = 1,16
    do
        gDestStatus[chan] = {}
        gDestStatus[chan].cc = {}
        gDestStatus[chan].pp = {}
        gDestStatus[chan].notes = 0
        gDestStatus[chan].lastSample = 0
        gDestStatus[chan].at = nil
        gDestStatus[chan].pb = nil
    end

end

--------------------------------------------------
-- handleNoteOn method handles incoming NoteOn msg
--------------------------------------------------

function handleNoteOn(msg)

    local ds = gDestStatus[msg:getChannel()]
    ds.notes = ds.notes + 1
    ds.lastSample = 0

    -- chase CC, PB, AT and PP
    if(gFollowCC) then
        chaseCC(msg)
    end
    if(gFollowPP) then
        chasePP(msg)
    end
    if(gFollowPB) then
        chasePB(msg)
    end
    if(gFollowAT) then
        chaseAT(msg)
    end

    scheduleMidi(msg)

end

-----------------------------------------------------
-- handleNoteOff method handles incoming NoteOff msg
-----------------------------------------------------

function handleNoteOff(msg)
    local chan = msg:getChannel()
    local ds = gDestStatus[chan]
    ds.notes = ds.notes - 1
    if(ds.notes == 0) then
        -- TODO is this correct?
        local transport = plugin.getCurrentPosition()
        ds.lastSample = transport.timeInSamples + msg:getPosition()
    end

    scheduleMidi(msg)

end

-----------------------------
-- Handle Expression msgs
-----------------------------
function handleExpression(msg)

    -- Make sure following is enabled
    if ( not isFollowEnabled(msg)) then
        -- send msg
        scheduleMidi(msg)
        setStatus(gSrcStatus, msg) 
        return
    end

    -- forward to all needed channels
    followChannels(msg)

    -- updating the received running value
    setStatus(gSrcStatus, msg)
end

--------------------------------------------------
-- followChannels
--
-- method will look at all
-- channels where msgs might be sustaining
-- and send the given decoration msg to them
-- if so, otherwise, stash the value for future
-- chasing, only look through current port
--------------------------------------------------
function followChannels(exprMsg)

    -- always send this expression even through to its current
    -- channel.  With DP it has already been channelized, with
    -- non-DP still needs to be channelized, which means it 
    -- will be duplicated.  Might want to change this so that
    -- in DP Mode, its sent here, but in non-DP mode we only 
    -- send here when it never gets channelized below.

    scheduleMidi(exprMsg)
    local chan = exprMsg:getChannel()
    local ds = gDestStatus[chan]
    setStatus(ds, exprMsg)

    -- TODO is this correct?
    local transport = plugin.getCurrentPosition()
    local samples = transport.timeInSamples + exprMsg:getPosition()
 
    -- now in order to handle possibility of chord, Look for more
    -- channels currently sustaining.

    -- TODO look for opportunity to keep raw midi information in status
    --      so that we can quickly create new midi msgs.

    for chan = 1,16 
    do
        if(chan ~= exprMsg:getChannel() and
                isSustaining(chan, samples)) then

            forwardToChan(exprMsg, chan)

        end
    end
end

------------------------------------------------------
-- forward a midi expression event to a given channel
-- but only if it is a different value then the current
-- running value on that channel
------------------------------------------------------
function forwardToChan(exprMsg, chan)

    local ds = gDestStatus[chan]

    if(exprMsg:isControl()) then
        local number = exprMsg:getNumber()
        local value = exprMsg:getValue()

        if(value ~= ds.cc[number]) then
            local msg = midi.Event.control( chan, number, value,
                                            exprMsg:getPosition())
            table.insert(gOutput, msg)
            setStatus(ds, msg)
        end


    elseif(exprMsg:isPitchBend()) then
        local pitch = exprMsg:getValue()
        if(pitch ~= ds.pb) then
            local msg = midi.Event.pitchBend( chan, pitch, exprMsg:getPosition())
            table.insert(gOutput, msg)
            setStatus(ds, msg)
        end

    elseif(exprMsg:isChannelPressure()) then
        local value = exprMsg:getValue()
        if(value ~= ds.at) then
            local msg = midi.Event.channelPressure( chan, value,
                                          exprMsg:getPosition())
            table.insert(gOutput, msg)
            setStatus(ds, msg)
        end

    elseif(exprMsg:isPolyPressure()) then
        local pitch = exprMsg:getNote()
        local value = exprMsg:getValue()
        if(pitch ~= ds.pp) then
           
            local msg = midi.Event.polyPressure( chan, pitch, value,
                                          exprMsg:getPosition())
            table.insert(gOutput, msg)
            setStatus(ds, msg)
        end
    end
end


-------------------------------------------------------
-- Chase functions for each expression midi type
-------------------------------------------------------

function chaseCC(msg)

    local chan = msg:getChannel()
    local ds = gDestStatus[chan]

    for num,sval in pairs(gSrcStatus.cc)
    do
        local dv = ds.cc[num]

        if(sval ~= nil and sval ~= dv) then
            local oMsg = midi.Event.control( chan, num, sval, msg:getPosition())
            table.insert(gOutput, oMsg)
            ds.cc[num] = sval
        end
    end
end

function chasePP(msg)

    local chan = msg:getChannel()
    local ds = gDestStatus[chan]

    for pitch,sval in pairs(gSrcStatus.cc)
    do
        local dv = ds.pp[pitch]

        if(sval ~= nil and sval ~= dv) then
            local oMsg = midi.Event.polyPressure( chan, pitch,
                                                  sval, msg:getPosition())
            table.insert(gOutput, oMsg)
            ds.pp[pitch] = sval
        end
    end
end

function chaseAT(msg)

    local chan = msg:getChannel()
    local ds = gDestStatus[chan]
    local at = gSrcStatus.at

    if(at ~= nil and at ~= ds.at) then
        local oMsg = midi.Event.channelPressure( chan, at, msg:getPosition())
        table.insert(gOutput, oMsg)
        ds.at = at
    end
end

function chasePB(msg)

    local chan = msg:getChannel()
    local ds = gDestStatus[chan]
    local pb = gSrcStatus.pb

    if(pb ~= nil and pb ~= ds.pb) then
        local oMsg = midi.Event.pitchBend( chan, pb, msg:getPosition())
        table.insert(gOutput, oMsg)
        ds.pb = pb
    end
end


-----------------------------------------------
-- isSustaining method
-----------------------------------------------

function isSustaining(chan, samplesNow)

    local ds = gDestStatus[chan]

    if ds.notes > 0 then
        return true
    end
    if ds.lastSample == 0 then
        return false
    end

    -- TODO temporary hack until we can get the global set during init
    if gContinuation == nil then
        if gSampleRate == nil then
            gSampleRate = plugin.getSampleRate()
        end
        gContinuation = 100 / 1000 * gSampleRate
    end

    if ((samplesNow - ds.lastSample) < gContinuation) then
        return true
    else
        return false
    end
end

----------------------------------------------
-- isFollowEnabled, check UI per event type
----------------------------------------------
function isFollowEnabled(msg)

    if msg:isControl() then
        return gFollowCC
    elseif msg:isChannelPressure() then
        return gFollowAT
    elseif msg:isPolyPressure() then
        return gFollowPP
    elseif msg:isPitchBend() then
        return gFollowPB
    else 
        return false
    end
end


--------------------------------------------------
-- Set the running status to the given status
-- object which could be the src status or could
-- be deststatus by channel.
--------------------------------------------------
function setStatus(status, msg)

    if msg:isControl() then
        local number = msg:getNumber()
        status.cc[number] = msg:getValue()

    elseif msg:isPolyPressure() then
        local pitch = msg:getNote()
        status.pp[pitch] = msg:getValue()

    elseif msg:isChannelPressure() then
        status.at = msg:getValue()

    elseif msg:isPitchBend() then
        status.pb = msg:getValue()
    end
end


---------------------------------------------
-- Funcction to add midi event to output
-- table.  Need to find a way to make this
-- easier so that this is not needed TODO
-- probably improve the underly message API
---------------------------------------------

function scheduleMidi(msg) 
    if msg:isNoteOn() then
        table.insert(gOutput, midi.Event.noteOn(
                    msg:getChannel(),
                    msg:getNote(),
                    msg:getVelocity(),
                    msg:getPosition()
                    ))
    elseif msg:isNoteOff() then
        table.insert(gOutput, midi.Event.noteOff(
                    msg:getChannel(),
                    msg:getNote(),
                    msg:getVelocity(),
                    msg:getPosition()
                    ))
    elseif msg:isControl() then
        table.insert(gOutput, midi.Event.control(
                    msg:getChannel(),
                    msg:getNumber(),
                    msg:getValue(),
                    msg:getPosition()
                    ))
    elseif msg:isPitchBend() then
        table.insert(gOutput, midi.Event.pitchBend(
                    msg:getChannel(),
                    msg:getValue(),
                    msg:getPosition()
                    ))
    elseif msg:isChannelPressure() then
        table.insert(gOutput, midi.Event.channelPressure(
                    msg:getChannel(),
                    msg:getValue(),
                    msg:getPosition()
                    ))
    elseif msg:isPolyPressure() then
        table.insert(gOutput, midi.Event.polyPressure(
                    msg:getChannel(),
                    msg:getNote(),
                    msg:getValue(),
                    msg:getPosition()
                    ))
    elseif msg:isProgram() then
        table.insert(gOutput, midi.Event.program(
                    msg:getChannel(),
                    msg:getNumber(),
                    msg:getPosition()
                    ))
    end 
end

---------------------------------------------
-- prepare callback
-- NOTE, ProtoPlug doens't have prepare 
-- callback so we just do it here at the end
-- of the global level code.  Hope this is ok
---------------------------------------------

function prepare(rate, block)
    print("calling prepare")
    gOutput = {}
    -- call the resetStatus when script loaded
    resetStatus()
end
plugin.addHandler("prepareToPlay", prepare)



--[[
    TODO

    - update comments to Kushview Lua standards, including for docs

    - get basic UI and params working properly

    -  get more sophisticated UI working 

    - sampleRate?

    - reset for logicPro?

    - POlyPressure needs to be fixed.

    - Need to figure out how to create midi objects

    - need to verify timestamping is correctly used

    - Need to find way to avoid malloc/new and find out bottom line about
      how midimessages are copied into buffers, by reference or by value
      and same with the midibuffer itself when leaving the process method

--]]
