/**************************************************************************
 *
 * Channelizer, for LogicPro Scripter
 *
 * Version 3.2k
 *
 * Full Documentation found at the following link:
 *
 *      https://gitlab.com/dewdman42/Channelizer/-/wikis/home
 *
 *****************************************************************************/  



/***************************************************************
 ***************************************************************
 *************** DO NOT EDIT BELOW HERE ************************
 ***************************************************************
 ***************************************************************/

const MAXPORTS=48; // 48 ports supported by VePro.

// off by default
// const CC99MODE = true;

var TOTALCHANS=MAXPORTS*16;

// TODO, can we find a way to do this without beatpos?  
var NeedsTimingInfo = true;  // needed for continuation


/****************************************
 * Check for secret CC99MODE feature
 ****************************************/

var uCC99Mode = false;
if( typeof CC99MODE != 'undefined') {
    uCC99Mode = CC99MODE;
}

/***********************************
 *
 * HandleMIDI()
 *
 ***/
function HandleMIDI(event) {

    if(uCC99Mode) {
        event.port = Data.lastCC99.get(event.channel) + 1;

        if(event instanceof ControlChange && event.number == 99) {
            Data.lastCC99.set(event.value, event.channel, 1);
            return;    // eat the CC99
        }
    }

    /*********************************************************
     * just in case not using AU3, default port to 1.
     *********************************************************/

    if(event.port == undefined || event.port < 1) {
        event.port = 1;
    }

    /*********************************************************
     * if not above border for channelizing, just send it now
     *********************************************************/

    var lowest = GuiParameters.get(LOWESTPORT);
    if(event.port < lowest || lowest==0) {
        event.sendWithCC99();
        return;
    }

    /********************************************
     * stash the srcStatus into the event
     * channelize by artid, event.send() is inside
     * make sure to remove the reference to encourage
     * garbage collection
     ********************************************/

    event.srcStatus = Data.SrcStatus.get(event.channel, event.port);
    event.channelize();
    event.srcStatus = undefined;
}


/************************************************************
 ************************************************************
 *             Event.prototype extensions
 ************************************************************
 ****************************************************G********/

/***************************************
 * Event.channelize()
 *
 * Method that will attempt to channelize the incoming event
 * based on articulationID, if present.  Notes are channelized,
 * other event types are optionally propagated
 * 
 ***/
Event.prototype.channelize = function() {

    this.srcPort = this.port;  
    this.srcChan = this.channel;

    /***************************************************
     * If there is no articulationID, then just go ahead
     * and send it, mostly non-notes propagation.
     ***************************************************/

    if (this.articulationID == undefined || this.articulationID < 1 ) {
   
        /* TODO, we should improve this so that when missing articulationID
         *       use the last known articulationID from this source track
         *       to channelize.  But..  only for notes.  For non-notes 
         *       we probabaly do not do that, just pass through
         */
        this.sendChannelized();
        return;
    }
    
    /****************************************************
     * register channel of incoming event as "source"
     * Only NoteOn/OFF will be registered
     ****************************************************/

    this.registerSource();

    /****************************************************
     * Verify channelize is ok and send if so
     * Only NoteOn/Off will be verified.
     ****************************************************/

    if(this.calculateChannel()==true) {
        this.sendChannelized();
    }
};


/***************************************
 * Event.registerSource()
 *
 * If the current event is arriving on a channel that we haven't
 * seen before, then register that channel as a "source"
 *
 ***/
Note.prototype.registerSource = function() {
    
    // If not already registered, then configure as source channel
    if( this.srcStatus.active != true ) {

        // flag this channel as a source channel
        this.srcStatus.active = true;
        
        // make sure record channel range before this one has new range
        this.adjustRangesBelow();
    }
};

Event.prototype.registerSource = function() {
    // nop, skip non-note events.
};


/**************************************************
 * Event.adjustLimitBelow()
 *
 * Adjust all the channels below this one until finding
 * the next lower source channel
 *
 ***/
Event.prototype.adjustRangesBelow = function() {

    var srcFlatChan = Data.FlatChannel(this.srcChan, this.srcPort);

    /* start with current port range */
    for(var chan=this.srcChan-1; chan>=1; chan--) {
        var ss = Data.SrcStatus.get(chan, this.port);
        var lowerFlatChan = Data.FlatChannel(chan, this.port);
        ss.range = srcFlatChan - lowerFlatChan;
        if(ss != undefined && ss.active == true) {
            return;
        }
    }

    /* remaining ports */
    for(var port=this.srcPort-1; port>=1; port--) {
        for(var chan=16; chan>=1; chan--) {
            var ss = Data.SrcStatus.get(chan, port);
            var lowerFlatChan = Data.FlatChannel(chan, port);
            ss.range = srcFlatChan - lowerFlatChan;
            if(ss != undefined && ss.active == true) {
                return;
            }
        }
    }
};


/*****************************************************
 * Event.calculateChannel()
 *
 * Calculate channel to send on, check for errors, return
 * true if good to go.  Otherwise return false.
 *
 ***/
Note.prototype.calculateChannel = function() {
    
    var srcFlatChan = Data.FlatChannel(this.srcChan, this.srcPort);

   /**************************************************
     * check for artid that is so far out of range
     * to cause major problems.
     * TODO how should we handle this?  mute for now
     **************************************************/

    if( (srcFlatChan + this.articulationID - 1) > TOTALCHANS) {
        console.log("ERROR: --> "+ this.myString());
        console.log("ERROR: articulationID:"+this.articulationID
        +" channelized out of range of all " + TOTALCHANS + " channels");
        console.log(" ");
        return false;
    }

    /*****************************************************************
     * Calculate channel to send
     *****************************************************************/

    var outChan = Data.Flat2Channel(srcFlatChan + this.articulationID - 1);
    var outPort = Data.Flat2Port(srcFlatChan + this.articulationID - 1);
 
    /******************************************************
     * check the range to see if conflict with other tracks
     ******************************************************/

    var outFlatChan = Data.FlatChannel(outChan, outPort);
    if( outFlatChan - srcFlatChan < this.srcStatus.range ) {
        this.channel = outChan;
        this.port = outPort;
        return true;
    }

    /***************************************************
     * report range conflict
     ***************************************************/

    else {
        console.log("WARNING: -->"+this.myString());
        console.log("WARNING: articulationID [" + this.articulationID
          + "] channelizing exceeds range of "+this.srcStatus.range+" channels");
    }

    /**************************************************
     * Overlap conflict detected, report details and 
     * re-channelize according to GUI settings
     **************************************************/
     
    var overlap = GuiParameters.get(RANGECONFLICT);
    
    var status = false;        
    switch(overlap) {
      case 0:
          console.log("WARNING: Muting channel overlap");              
          status = false;
          break;
      
      case 1:
          this.port = this.srcPort;
          this.channel = this.srcChan;
          console.log("WARNING: Sending channel-overlapped note to source channel");
          console.log("WARNING: -----> "+ this.myString());
          status = true;
          break;
      
      case 2:
          console.log("WARNING: Sending channel-overlapped note thru as requested");
          console.log("WARNING: -----> "+ this.myString());
          status = true;
          break;       
          
      default:
          console.log("ERROR: invalid Channel Range overlap");
          status = false;
          break;
    }
    
    console.log(" ");
    
    return status;
};

Event.prototype.calculateChannel = function() {
    //  nop - skip non-notes
    return true;
};


/***********************************************************
 * Event.sendChannelized()
 *
 * Non-Notes sent with this in order to be propagated.
 *
 ***/
Event.prototype.sendChannelized = function() {

    /************************************************
     * See if GUI allows following of this event type
     ************************************************/

    if (this.IsFollowEnabled() != 1) {
        this.sendWithCC99(); // since not following channels, send thru
        // this.setSrcRunning(); // TODO, might not need this
        return;
    }

    /************************************************
     * Keep Track of max channlization so far
     ************************************************/

    this.setMax();

    /**************************************************
     * If this CC(etc) came in on a source channel
     * then propagate it to related sustaining channels
     **************************************************/

    if (this.srcStatus.active) {
        this.propagate();
        this.setSrcRunning();
    }

    /************************************************
     * If not on a source channel, then pass through
     * allow artid to channelize, rare case
     ************************************************/

    else {
        this.setDestRunning(); // TODO might not need this ??
        this.sendWithCC99(); // ??
    }

};

/*********************************************
 * Event.sendChannelized()
 *
 * noteOn method handles incoming NoteOn msg
 *
 ***/
NoteOn.prototype.sendChannelized = function() {

    this.setMax();

    var ds = this.getDestStatus();
    ds.notes++;
    ds.lastBeat = 0; // reset the continuation time

    /*********************************************************
     * NoteOn being channelized, first chase CC(etc) events
     * which have been accumulating if this new channel didn't
     * have sustaining notes before
     *********************************************************/

    if (GuiParameters.get(FORWARDPC) == 1) this.chasePC();
    if (GuiParameters.get(FORWARDCC) == 1) this.chaseCC();
    if (GuiParameters.get(FORWARDPB) == 1) this.chasePB();
    if (GuiParameters.get(FORWARDCP) == 1) this.chaseCP();

    this.sendWithCC99();
};

/*********************************************
 * Event.sendChannelized()
 *
 * noteOff method handles incoming NoteOff msg
 *
 ***/
NoteOff.prototype.sendChannelized = function() {

    this.setMax();
    var ds = this.getDestStatus();
    ds.notes--;
    if (ds.notes <= 0) {
        ds.lastBeat = this.beatPos;
    }
    this.sendWithCC99();
};


/*********************************************
 * setMax method
 *
 * determine the highest channel in use in order
 * to avoid useless scanning during PLAY
 *
 *********************************************/
Event.prototype.setMax = function() {
    var flatChan = Data.FlatChannel(this.channel, this.port);
    var srcFlatChan = Data.FlatChannel(this.srcChan, this.srcPort);
    var reach = flatChan - srcFlatChan;
    if (reach > this.srcStatus.max) {
        this.srcStatus.max = reach;
    }
};


/*********************************************
 * Event.chaseCC()
 *
 * chasing methods handling chasing for each 
 * event type
 *
 ***/
Event.prototype.chaseCC = function() {

    /************************************************
     * TODO, how to handle not created yet here?
     ************************************************/

    var ds = this.getDestStatus();

    for (var ccnum in this.srcStatus.srcCC) {
        if (this.srcStatus.srcCC[ccnum] != undefined &&
                this.srcStatus.srcCC[ccnum] != ds.destCC[ccnum]) {

            Data.CC.number = Number(ccnum);
            Data.CC.value = this.srcStatus.srcCC[ccnum];
            Data.CC.chase(this);

            ds.destCC[ccnum] = Data.CC.value;
        }
    }
};

/*********************************************
 * Event.chaseCP()
 *
 ***/
Event.prototype.chaseCP = function() {

    var ds = this.getDestStatus();

    if (this.srcStatus.srcCP != undefined && this.srcStatus.srcCP != ds.destCP) {
        Data.CP.value = this.srcStatus.srcCP;
        Data.CP.chase(this);

        ds.destCP = Data.CP.value;
    }
};

/*********************************************
 * Event.chasePB()
 *
 ***/
Event.prototype.chasePB = function() {

    var ds = this.getDestStatus();

    if (this.srcStatus.srcPB != undefined && this.srcStatus.srcPB != ds.destPB) {
        Data.PB.value = this.srcStatus.srcPB;
        Data.PB.chase(this);

        ds.destPB = Data.PB.value;
    }
};

/*********************************************
 * Event.chasePC()
 *
 ***/
Event.prototype.chasePC = function() {

    var ds = this.getDestStatus();

    if (this.srcStatus.srcPC != undefined && this.srcStatus.srcPC != ds.destPC) {
        Data.PC.number = this.srcStatus.srcPC;
        Data.PC.chase(this);

        ds.destPC = Data.PC.number;
    }
};


/************************************************
 * Event.chase()
 *
 * chase method is used only during chasing where
 * the timing needs to match the note
 *
 **/
Event.prototype.chase = function(note) {
    this.beatPos = note.beatPos;
    this.channel = note.channel;
    this.port = note.port;
    this.sendWithCC99();
};


/*********************************************
 *
 * Event.propagate()
 *
 * method will look at all
 * channels where events might be sustaining
 * and send the given expression event to them
 * if so, otherwise, stash the value for future
 * chasing
 *
 ****/
Event.prototype.propagate = function() {

    /*****************************************************
     * for now assume this is non-channelized CC, PB, etc.
     *****************************************************/

    var sent = false;
    var origPort = this.port;
    var origChan = this.channel;

    var startFlatChan = Data.FlatChannel(this.srcStatus.channel, 
                                           this.srcStatus.port);
    var maxFlatChan = startFlatChan + this.srcStatus.max;

    for(var i=startFlatChan; i<=maxFlatChan; i++) {

        var chan = Data.Flat2Channel(i);
        var port = Data.Flat2Port(i);

        if (Data.IsSustaining(chan, port, this.beatPos)) {

            if( !this.isAlreadyRunning(chan, port)) {

                this.port = port;
                this.channel = chan;
                this.sendWithCC99();
                this.setDestRunning();
                sent = true;
            }
        }
    }

    this.port = origPort;
    this.channel = origChan;

    /*****************************************************
     * if no channelizing happens, then send to the source
     * channel right now
     * TODO make sure this is the right thing to do
     *****************************************************/

    if (!sent) {
        this.sendWithCC99();
        this.setDestRunning();
    }
};


/**************************************************
 * wrapper around send function in order to 
 * optionally send CC99 encoding
 **************************************************/

var cc99 = new ControlChange;
cc99.number = 99;

Event.prototype.sendWithCC99 = function() {

    if(uCC99Mode) {
        cc99.value = this.port - 1;
        cc99.channel = this.channel;
        cc99.port = this.port;
        cc99.send();
    }

    this.send();
};

/*****************************************************
 * Event.isAlreadyRunning()
 *
 * method per event type, this=event being propogated
 *
 *****************************************************/

Event.prototype.isAlreadyRunning = function(chan, port) {
    return true; // noop
};

ControlChange.prototype.isAlreadyRunning = function(chan, port) {
    var ds = Data.DestStatus.get(chan, port);
    if( ds.destCC[this.number] == this.value ) {
        return true;
    }
    else {
        return false;
    }
};

ChannelPressure.prototype.isAlreadyRunning = function(chan, port) {
    var ds = Data.DestStatus.get(chan, port);
    if( ds.destCP == this.value ) {
        return true;
    }
    else {
        return false;
    }
};

PitchBend.prototype.isAlreadyRunning = function(chan, port) {
    var ds = Data.DestStatus.get(chan, port);
    if( ds.destPB == this.value ) {
        return true;
    }
    else {
        return false;
    }
};

ProgramChange.prototype.isAlreadyRunning = function(chan, port) {
    var ds = Data.DestStatus.get(chan, port);
    if( ds.destPC == this.number ) {
        return true;
    }
    else {
        return false;
    }
};


/*****************************************************
 * setSrcRunning()  - method per event type
 *
 ***/

Event.prototype.setSrcRunning = function() {
    return; // noop
};

ControlChange.prototype.setSrcRunning = function() {
    this.srcStatus.srcCC[this.number] = this.value;
};
ChannelPressure.prototype.setSrcRunning = function() {
    this.srcStatus.srcCP = this.value;
};
PitchBend.prototype.setSrcRunning = function() {
    this.srcStatus.srcPB = this.value;
};
ProgramChange.prototype.setSrcRunning = function() {
    this.srcStatus.srcPC = this.number;
};

/************************************************
 * setDestRunning() -  method per event type
 *
 ************************************************/

Event.prototype.setDestRunning = function() {
    return; // noop
};
ControlChange.prototype.setDestRunning = function() {
    var ds = this.getDestStatus();
    ds.destCC[this.number] = this.value;
};
ChannelPressure.prototype.setDestRunning = function() {
    var ds = this.getDestStatus();
    ds.destCP = this.value;
};
PitchBend.prototype.setDestRunning = function() {
    var ds = this.getDestStatus();
    ds.destPB = this.value;
};
ProgramChange.prototype.setDestRunning = function() {
    var ds = this.getDestStatus();
    ds.destPC = this.number;
};

/**************************************************
 * isFollowEnabled()
 *
 * check GUI option per event type
 * static methods
 * TODO, what about PolyPressure?
 *
 **************************************************/

ControlChange.prototype.IsFollowEnabled = function() {
    return GuiParameters.get(FORWARDCC);
};
ChannelPressure.prototype.IsFollowEnabled = function() {
    return GUiParameters.get(FOLLOWCP);
};
PitchBend.prototype.IsFollowEnabled = function() {
    return GuiParameters.get(FORWARDPB);
};
ProgramChange.prototype.IsFollowEnabled = function() {
    return GuiParameters.get(FORWARDPC);
};


/***************************************************
 * helper functions that return the srcStatus or
 * destDstatus of the current chan/port
 ***************************************************/

Event.prototype.getSrcStatus = function() {
    return Data.SrcStatus.get(this.channel, this.port);
};

Event.prototype.getDestStatus = function() {
    return Data.DestStatus.get(this.channel, this.port);
};


/*************************
 *   ____ _   _ ___ 
 *  / ___| | | |_ _|
 * | |  _| | | || | 
 * | |_| | |_| || | 
 *  \____|\___/|___|
 *
 *************************/


var PluginParameters = [];

PluginParameters.push({
    name: "————— Channelizer —————",
    type: "text",
});

var portNames = [];
portNames.push("(Disabled)");
for(var fc=1;fc<=MAXPORTS;fc++) {
    portNames.push("port: "+fc);
}

PluginParameters.push({
    name: "Lowest Channelizer Port",
    type: "menu",
    valueStrings: portNames,
    defaultValue: 1,
    disableAutomation: true
});
const LOWESTPORT = PluginParameters.length-1;

PluginParameters.push({
    name: "Channel Range Conflict",
    type: "menu",
    valueStrings: ["Mute","Send to Source Channel","Allow"],
    defaultValue: 0,
    disableAutomation: true
});
const RANGECONFLICT = PluginParameters.length-1;

PluginParameters.push({
    name: "———— Event Forwarding ————",
    type: "text",
});

PluginParameters.push({
    name: "Forward CC",
    type: "checkbox",
    defaultValue: 1,
    disableAutomation: true
});
const FORWARDCC = PluginParameters.length-1;

PluginParameters.push({
    name: "Forward PitchBend",
    type: "checkbox",
    defaultValue: 1,
    disableAutomation: true
});
const FORWARDPB = PluginParameters.length-1;

PluginParameters.push({
    name: "Forward ChannelPressure",
    type: "checkbox",
    defaultValue: 0,
    disableAutomation: true
});
const FORWARDCP = PluginParameters.length-1;

PluginParameters.push({
    name: "Forward ProgramChange",
    type: "checkbox",
    defaultValue: 0,
    hidden: false,
    disableAutomation: true
});
const FORWARDPC = PluginParameters.length-1;


PluginParameters.push({
    name: "Continuation",
    type: "lin",
    minValue: 0,
    maxValue: 10,
    numberOfSteps: 1000,
    defaultValue: 0.20,
    unit: "beats",
    disableAutomation: true
});
const CONTINUATION = PluginParameters.length-1;


PluginParameters.push({
    name: "RESET",
    type: "momentary",
    disableAutomation: true
});
const RESETBUTTON = PluginParameters.length-1;


/*********************************************
 * ChannelData class
 *********************************************/

function ChannelData() {
    this.data = new Array(17);
    for(var chan=0;chan<=16;chan++) {
        this.data[chan] = []; // port elements created on demand
    }
}

ChannelData.prototype.set = function(val, chan, port) {
    this.data[chan][port] = val;
};

ChannelData.prototype.get = function(chan, port) {
    return this.data[chan][port];
};


/******************************************************
 * alternative to Event.toString that includes port
 ******************************************************/

Event.prototype.myString = function() {
    var estr = this.toString();
    var firstSpace = estr.indexOf(" ");
    return estr.substr(0,firstSpace)+" port:"+this.port+estr.substr(firstSpace);
};


/*********************************************
 * Data Singletons
 *********************************************/


var Data = {
    SrcStatus: new ChannelData,
    DestStatus: new ChannelData,
    lastCC99: new ChannelData,

    CC: new ControlChange,
    CP: new ChannelPressure,
    PB: new PitchBend,
    PC: new ProgramChange,

    matrix: new ChannelData,
    flat: new Array(TOTALCHANS)
};

/********************************************************
 * Data.Init()
 *
 * TODO
 * Is any of this redundant with the reset function?
 ***/
Data.Init = function() {
    for(var chan=1;chan<=16;chan++) {

        this.lastCC99.set(0, chan, 1);

        for(var port=1;port<=MAXPORTS;port++) {

            /*****************************************
             * Init SrcStatus node
             *****************************************/

            var ss = {};
            ss.channel = chan;
            ss.port = port;
            ss.max = 0;
            ss.srcCC = [];
            ss.scrPB = undefined;
            ss.srcPC = undefined;
            ss.srcCP = undefined;
            ss.active = false;
            ss.range = 0;

            this.SrcStatus.set(ss, chan, port);

            /*****************************************
             * Init DestStatus node
             *****************************************/

            var ds = {};
            ds.channel = chan;
            ds.port = port;
            ds.notes = 0;
            ds.destCC = [];
            ds.destPB = undefined;
            ds.destPC = undefined;
            ds.destCP = undefined;
            ds.lastBeat = 0;
            
            this.DestStatus.set(ds, chan, port);
        }
    }

    /**************************************
     * init the flat channel list converter
     **************************************/

    var i=1;
    for(var port=1; port<=MAXPORTS; port++) {
        for(var chan=1; chan<=16; chan++) {

            this.matrix.set(i, chan, port);

            var node = {};
            node.port = port;
            node.channel = chan;
            this.flat[i] = node;

            i++;
        }
    }

    this.Reset();
};

/********************************************************
 * Event.Reset()
 *
 * Called during Reset() to reset all the status buffers
 * static method
 *
 ****/
Data.Reset = function() {

    for(var chan=1;chan<=16;chan++) {

        // reset port for each chan
        this.lastCC99.set(0, chan, 1);

        for(var port=1;port<=MAXPORTS;port++) {

            /*******************************
             * reset SrcStatus buffers
             *******************************/

            var ss = this.SrcStatus.get(chan, port);

            ss.max = 0;
            if(ss.srcCC != undefined) {
                delete ss.srcCC;
            }
            ss.srcCC = [];
            ss.scrPB = undefined;
            ss.srcPC = undefined;
            ss.srcCP = undefined;

            ss.active = false;
            ss.range = (TOTALCHANS) - this.FlatChannel(chan, port) + 1;

            /*******************************
             * reset DestStatus buffers
             *******************************/

            var ds = this.DestStatus.get(chan, port);
            ds.notes = 0;
            if(ds.destCC != undefined) {
                delete ds.destCC;
            }
            ds.destCC = [];
            ds.destPB = undefined;
            ds.destPC = undefined;
            ds.destCP = undefined;
            ds.lastBeat = 0;
        }
    }
};


/**********************************************
 * Channel number conversion functions 
 * back and forth between flat list and 
 * port/chan matrix
 **********************************************/

Data.FlatChannel = function(chan, port) {
    return this.matrix.get(chan, port);
};

Data.Flat2Port = function(flatChan) {
    if(flatChan > TOTALCHANS || flatChan < 1) {
        console.log("ERROR: Data.Flat2Port: invalid FlatChan("+flatChan+")");
        return 0;
    }
    return this.flat[flatChan].port;
};

Data.Flat2Channel = function(flatChan) {
    if(flatChan > TOTALCHANS || flatChan < 1) {
        console.log("ERROR: Data.Flat2Channel: invalid FlatChan("+flatChan+")");
        return 0;
    }
    return this.flat[flatChan].channel;
};


/*********************************************
 * Data.IsSustaining 
 *
 * returns if there are any notes sustaining
 * on the given chan/port
 *
 ***/
Data.IsSustaining = function(chan, port, beatPosNow) {
    var ds = this.DestStatus.get(chan, port);

    if (ds.notes > 0) {
        return true;
    }
    if (beatPosNow == undefined || beatPosNow == 0) {
        return false;
    }
    if (beatPosNow - ds.lastBeat < GuiParameters.get(CONTINUATION)) {
        return true;
    }
    else {
        return false;
    }
};


/************************************
 * GUI Caching
 ************************************/

var GuiParameters = {
    data: [],
    set: function(id, val) {
        this.data[id] = val;
    },
    get: function(id) {
        if(this.data[id] == undefined) {
            this.data[id] = GetParameter(id);
        }
        return this.data[id];
    }
};


/******************************************************
 * buffered logging
 ******************************************************/
var console = {
    maxFlush: 20,
    b:[],
    log: function(msg) {this.b.push(msg)},
    flush: function() {
        var i=0;
        while(i<=this.maxFlush && this.b.length>0) {
            Trace(this.b.shift());
            i++;
        }
    }
};

/**************************************************************
 **************************************************************
 **************************  CALLBACKS ************************
 **************************************************************
 **************************************************************/


function Initialize() {
    Data.Init();
}

/***********************************
 * Reset()
 ***/
function Reset() {
    Data.Reset();
}

/*******************************************
 * Idle
 *******************************************/
function Idle() {
    console.flush();
}

/***********************************
 * ParameterChanged()
 ***/
function ParameterChanged(id, val) {

    GuiParameters.set(id, val);

    // Handle RESET button
    if (id == RESETBUTTON) {
        Data.Reset();
    }
}
