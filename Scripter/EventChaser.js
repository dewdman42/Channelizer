/**************************************************************************
 *
 * EventChaser.js for LPX Scripter 
 *
 * Version 3.00
 *
 * This script will forward CC, PitchBend and ChannelPressure aftertouch
 * messages to all channels between 1-127 where notes are currently 
 * active (and after NoteOff for a short continuation period).
 *
 * Any input events from any channel will be forwarded.
 *
 *************************************************************************/


var MAXPORTS = 48;     // max ports supported by VePro
//var CHASEPORTS = [1,5];  // example multi-ports 1 and 5

/**************************************************************
 ************** DO NOT EDIT BELOW HERE ************************
 **************************************************************/

var NeedsTimingInfo = true;
var PluginParameters = [];
var CHASEPORTS;

/**********************************************
 * Some Global variables and structures
 **********************************************/

// reusable events for sending chased events
this.gCC = new ControlChange;
this.gCP = new ChannelPressure;
this.gPB = new PitchBend;
this.gPP = new PolyPressure;

// array of ports, one configuration per port
var gPortStatus = new Array(MAXPORTS+1);

for(let p=1; p<gPortStatus.length; p++) {
    gPortStatus[p]= {};
    gPortStatus[p].chase = false;
}

// if CHASEPORTS was not defined, default to port 1 only
if(CHASEPORTS == undefined) {
    gPortStatus[1].chase = true;
}

// Otherwise use the defined entries in CHASEPORTS array
else {
    for(let cp=0; cp<CHASEPORTS.length; cp++) {
        let port = CHASEPORTS[cp];
        gPortStatus[port].chase = true;
    }
}

// reset status
resetStatus();


/************************************************************
 * resetStatus method called by Reset callback 
 * resets all running status
 ************************************************************/

function resetStatus() {

    for(let p=1; p<=MAXPORTS; p++) {

        let ps = gPortStatus[p];
        ps.srcStatus = {};
        ps.destStatus = new Array(17);
        for (let chan=1; chan<=16; chan++) {
            ps.destStatus[chan] = {};
        }     
          
        // srcStatus reset
        ps.srcStatus.cc = [];
        ps.srcStatus.ccnumlist = [];
        ps.srcStatus.pp = [];
        ps.srcStatus.ppnumlist = [];

        // destStatus reset on 16 channels
        for(let chan=1; chan<=16; chan++) {
            let d = ps.destStatus[chan];
            d.notes = 0;
            delete d.cc;
            delete d.ccnumlist;
            delete d.pp;
            delete d.ppnumlist;
            d.ccnumlist = [];
            d.cc = [];
            d.ppnumlist = [];
            d.pp = [];
            d.lastBeat = 0;
        }
    }
}

/**************************************************
 * status accessor functions
 **************************************************/

function getDtx(port, chan) {
    return gPortStatus[port].destStatus[chan];
};

function getStx(port) {
    return gPortStatus[port].srcStatus;
};

function chasePort(port) {
    return gPortStatus[port].chase;
}

/*********************************************
 * isSustaining method
 *********************************************/
function isSustaining(port, chan, beatPosNow) {

    let destStatus = getDtx(port, chan);

    if (destStatus.notes > 0) {
        return true;
    }
    if (beatPosNow == undefined || beatPosNow == 0) {
        return false;
    }
    if (beatPosNow - destStatus.lastBeat < GUIcontinuation()) {
        return true;
    }
    else {
        return false;
    }
}

/*********************************************
 * noteOn method handles incoming NoteOn msg
 *********************************************/
function noteOn(event) {

    let destStatus = getDtx(event.port, event.channel);
    destStatus.notes++;
    destStatus.lastBeat = 0; // reset the continuation time

    // Chase CC, PB and AT events for this NoteOn
    if (GUIfollowCC()) chaseCC(event);
    if (GUIfollowPB()) chasePB(event);
    if (GUIfollowAT()) chaseAT(event);
    if (GUIfollowPP()) chasePP(event);

    event.send();
}

/*********************************************
 * noteOff method handles incoming NoteOff msg
 *********************************************/
function noteOff(event) {
    let destStatus = getDtx(event.port, event.channel);
    destStatus.notes--;
    if (destStatus.notes == 0) {
        destStatus.lastBeat = event.beatPos;
    }

    event.send();
}

/*********************************************
 * follow method handles incoming CC, AT, PB
 *********************************************/
function followExpression(event) {

    // Make sure following is enabled
    if (!event.isFollowEnabled()) {
        event.send(); // since not following channels, send thru
        event.setSrcRunning(); // TODO, might not need this
        return;
    }

    // forward to all needed channels
    followChannels(event);

    // updating the received running value
    event.setSrcRunning();

}

/*********************************************
 *
 * followChannels
 *
 * method will look at all
 * channels where events might be sustaining
 * and send the given decoration event to them
 * if so, otherwise, stash the value for future
 * chasing, only look through current port
 *********************************************/

function followChannels(event) {

    // for now assume this is a non-channelized CC, PB, AT, PP event

    let sent = false;
    let origChan = event.channel;

    for(let chan=1; chan<=16; chan++) {

        if (isSustaining(event.port, chan, event.beatPos) 
                && !event.isAlreadyRunning(event.port, chan)) {

            event.channel = chan;
            event.send();
            event.setDestRunning();
            sent = true;
        }
    }

    event.channel = origChan;

    // if no channelizing happens, then send to the source channel right now
    if (!sent) {
        event.send();
        event.setDestRunning();
    }
}


/*********************************************
 * chasing methods handling chasing for each 
 * event type
 *********************************************/

function chaseCC(event) {

    let destStatus = getDtx(event.port, event.channel);
    let srcStatus =  getStx(event.port);

    for (let c in srcStatus.ccnumlist) {
    
        let ccnum = srcStatus.ccnumlist[c];
        
        if (srcStatus.cc[ccnum] != destStatus.cc[ccnum]) {

            gCC.number = Number(ccnum);
            gCC.value = srcStatus.cc[ccnum];
            gCC.beatPos = event.beatPos;
            gCC.channel = event.channel;
            gCC.port = event.port;
            gCC.send();

            destStatus.cc[ccnum] = gCC.value;
        }
    }
}

function chasePP(event) {

    let destStatus = getDtx(event.port, event.channel);
    let srcStatus =  getStx(event.port);

    for (let p in srcStatus.ppnumlist) {
    
        let ppnum = srcStatus.ppnumlist[p];
        
        if (srcStatus.pp[ppnum] != destStatus.pp[ppnum]) {

            gPP.pitch = Number(ppnum);
            gPP.value = srcStatus.pp[ppnum];
            gPP.beatPos = event.beatPos;
            gPP.channel = event.channel;
            gPP.port = event.port;
            gPP.send();

            destStatus.pp[ppnum] = gPP.value;
        }
    }
}


function chaseAT(event) {

    let destStatus = getDtx(event.port, event.channel);
    let srcStatus =  getStx(event.port);
    
    if (srcStatus.at != undefined && srcStatus.at != destStatus.at) {
        gCP.value = srcStatus.at;
        gCP.beatPos = event.beatPos;
        gCP.channel = event.channel;
        gCP.port = event.port;
        gCP.send();

        destStatus.at = gCP.value;
    }
}

function chasePB(event) {

    let destStatus = getDtx(event.port, event.channel);
    let srcStatus = getStx(event.port);
    
    if (srcStatus.pb != undefined && srcStatus.pb != destStatus.pb) {
        gPB.value = srcStatus.pb;
        gPB.beatPos = event.beatPos;
        gPB.channel = event.channel;
        gPB.port = event.port;
        gPB.send();

        destStatus.pb = gPB.value;
    }
}



/**************************************************
 * check GUI option per event type
 **************************************************/

ControlChange.prototype.isFollowEnabled = function() {
    return GUIfollowCC();
};
PolyPressure.prototype.isFollowEnabled = function() {
    return GUIfollowPP();
};
ChannelPressure.prototype.isFollowEnabled = function() {
    return GUIfollowPB(this);
};
PitchBend.prototype.isFollowEnabled = function() {
    return GUIfollowAT(this);
};


/************************************************
 * setDestRunning method per event type
 ************************************************/

Event.prototype.setDestRunning = function() {
    return; // noop
};

ControlChange.prototype.setDestRunning = function() {
    let destStatus = getDtx(this.port, this.channel);
    if(destStatus.cc[this.number] == undefined) {
        destStatus.ccnumlist.push(this.number);
    }
    destStatus.cc[this.number] = this.value;
    
};
PolyPressure.prototype.setDestRunning = function() {
    let destStatus = getDtx(this.port, this.channel);
    if(destStatus.pp[this.pitch] == undefined) {
        destStatus.ppnumlist.push(this.pitch);
    }
    destStatus.pp[this.pitch] = this.value;
    
};
ChannelPressure.prototype.setDestRunning = function() {
    let destStatus = getDtx(this.port, this.channel);
    destStatus.at = this.value;
};

PitchBend.prototype.setDestRunning = function() {
    let destStatus = getDtx(this.port, this.channel);
    destStatus.pb = this.value;
};

/************************************************
 * setSrcRunning method per event type
 ************************************************/

Event.prototype.setSrcRunning = function() {
    return; // noop
};

ControlChange.prototype.setSrcRunning = function() {
    let srcStatus = getStx(this.port);
    if(srcStatus.cc[this.number] == undefined) {
        srcStatus.ccnumlist.push(this.number);
    }
    srcStatus.cc[this.number] = this.value;
};
PolyPressure.prototype.setSrcRunning = function() {
    let srcStatus = getStx(this.port);
    if(srcStatus.pp[this.pitch] == undefined) {
        srcStatus.ppnumlist.push(this.pitch);
    }
    srcStatus.pp[this.pitch] = this.value;
};
ChannelPressure.prototype.setSrcRunning = function() {
    let srcStatus = getStx(this.port);
    srcStatus.at = this.value;
};

PitchBend.prototype.setSrcRunning = function() {
    let srcStatus = getStx(this.port);
    srcStatus.pb = this.value;
};


/************************************************
 * isAlreadyRunning method per event type
 ************************************************/

Event.prototype.isAlreadyRunning = function(port, chan) {
    return true; // noop
};

ControlChange.prototype.isAlreadyRunning = function(port, chan) {

    let destStatus = getDtx(port, chan);
    let ccval =  destStatus.cc[this.number];

    if (ccval == this.value) {
        return true;
    }
    else {
        return false;
    }
};

PolyPressure.prototype.isAlreadyRunning = function(port, chan) {

    let destStatus = getDtx(port, chan);
    let ppval =  destStatus.pp[this.pitch];

    if (ppval == this.value) {
        return true;
    }
    else {
        return false;
    }
};

ChannelPressure.prototype.isAlreadyRunning = function(port, chan) {

    let destStatus = getDtx(port, chan);
    let atval =  destStatus.at;

    if ( atval == this.value) {
        return true;
    }
    else {
        return false;
    }
};

PitchBend.prototype.isAlreadyRunning = function(port, chan) {

    let destStatus = getDtx(port, chan);
    let pbval =  destStatus.pb;

    if ( pbval == this.value) {
        return true;
    }
    else {
        return false;
    }
};



/********************************************
 *            _ _ _                _        
 *   ___ __ _| | | |__   __ _  ___| | _____ 
 *  / __/ _` | | | '_ \ / _` |/ __| |/ / __|
 * | (_| (_| | | | |_) | (_| | (__|   <\__ \
 *  \___\__,_|_|_|_.__/ \__,_|\___|_|\_\___/
 *                                          
 ********************************************/

/*******************************************
 * HandleMIDI
 *******************************************/

function HandleMIDI(event) {

    /******************************************************
     * if port is not configured for chasing, then 
     * pass event through and get out
     ******************************************************/
   
    if(chasePort(event.port) != true) {
        event.send();
        return;
    }

    /*******************************************
     * pass PC through as-is
     *******************************************/

    if (event instanceof ProgramChange) {
        event.send();
        return;
    }

    if (event instanceof NoteOn) {
        noteOn(event);
        return;
    }

    if (event instanceof NoteOff) {
        noteOff(event);
        return;
    }

    /***************************
     * CC, AT and PB events
     ***************************/

    followExpression(event);
}


/*******************************************
 * Reset
 *******************************************/

function Reset() {
    resetStatus();
}


/*******************************************
 * ParameterChanged
 *******************************************/

function ParameterChanged(id, val) {
    GuiParameters.set(id, val);
}


/**************************
 *  ____ _   _ ___ 
 * / ___| | | |_ _|
 *| |  _| | | || | 
 *| |_| | |_| || | 
 * \____|\___/|___|
 *
 **************************/                 


PluginParameters.push({
    name: "Forward CC",
    type: "checkbox",
    defaultValue: 1,
    disableAutomation: true
});
PluginParameters.push({
    name: "Forward PitchBend",
    type: "checkbox",
    defaultValue: 1,
    disableAutomation: true
});
PluginParameters.push({
    name: "Forward AfterTouch",
    type: "checkbox",
    defaultValue: 1,
    disableAutomation: true
});
PluginParameters.push({
    name: "Forward PolyPressure",
    type: "checkbox",
    defaultValue: 1,
    disableAutomation: true
});
var CONTINUATION;
if (CONTINUATION == undefined) {
    CONTINUATION = 1.00;
}
PluginParameters.push({
    name: "Continuation",
    type: "lin",
    minValue: 0,
    maxValue: 10,
    numberOfSteps: 1000,
    defaultValue: CONTINUATION,
    unit: "beats",
    disableAutomation: true
});

var GuiParameters = {
    data: [],
    set: function(id, val) {
        this.data[id] = val;
    },
    get: function(id) {
        if(this.data[id] == undefined) {
            this.data[id] = GetParameter(id);
        }
        return this.data[id];
    }
};

function GUIfollowCC() {
    if (GuiParameters.get(0) == 1) {
        return true;
    }
    else {
        return false;
    }
}

function GUIfollowPB() {
    if (GuiParameters.get(1) == 1) {
        return true;
    }
    else {
        return false;
    }
}

function GUIfollowAT() {
    if (GuiParameters.get(2) == 1) {
        return true;
    }
    else {
        return false;
    }
}

function GUIfollowPP() {
    if (GuiParameters.get(3) == 1) {
        return true;
    }
    else {
        return false;
    }
}
function GUIcontinuation() {
    return GuiParameters.get(4);
}
