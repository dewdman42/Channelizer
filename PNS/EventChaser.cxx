/**************************************************************************
 *
 * EventChaser.cxx for BlueCatAudio PlugNScript
 *
 * Version 3.04
 *
 * This script will forward CC, PitchBend and ChannelPressure aftertouch
 * messages to all channels between 1-127 where notes are currently 
 * active (and after NoteOff for a short continuation period).
 *
 * Any input events from any channel will be forwarded.
 *
 * TODO, use with KUIML instead of default GUI and make it look less like
 *       bluecataudio
 *
 *       build as native C++ app, get properly authentication and VST id's
 *       in order to distribute as AUmfx, VST and VST3
 *
 *       eliminate any dependencies in the script for the include files
 *       where the user needs to ahve some folder layout.  Buidling it as
 *       VST and especially native would eliminate that problem
 *
 *       Should we also copy PC messages?  I think not for now.
 *
 *       I think this code can be simplified a bit, it was taken from 
 *       LogicPro script but think it through and simplify it if possible.
 *
 *    ***Handle cycle looping correctly.  What is correct?
 *
 *       do extensive testing to make absoluitely sure all expression types
 *       are being handled absolutely correctly in all scenarios.  
 * 
 *       Need to make sure the there are copy copy by reference happening 
 *       with reuse of global midi objects to avoid malloc.  
 *
 *************************************************************************/

#include "../library/Midi.hxx"

// filter interface-------------------------
string name        = "EventChaser";
string description = "Chase midi expression events across 16 channels";

array<string> inputParametersNames={"CC","PitchBend",
                                    "AfterTouch","PolyTouch","Continuation"};
array<double> inputParameters(inputParametersNames.length);
array<double> inputParametersDefault={1,1,1,1,50};
array<double> inputParametersMin={0, 0, 0, 0, 0}; 
array<double> inputParametersMax={1, 1, 1, 1, 1000};
array<string> inputParametersEnums={"Off;On", "Off;On", "Off;On", "Off;On",""};
array<int> inputParametersSteps={2, 2, 2, 2, 1001};
array<string> inputParametersUnits={"", "", "", "","ms"};
array<string> inputParametersFormats={"","","", "",".0"};

/*******************************************
 * Global variables to hold status
 *******************************************/

class Status {
    array<uint> cc(128,1000);  // use 1000 to represent "undefined"
    array<uint> ccnumlist;
    array<uint> pp(128,1000);  // use 1000 to represent "undefined"
    array<uint> ppnumlist;
    uint at;
    int pb;
    int notes;     // dest only
    double lastSample;  // dest only
};

Status        gSrcStatus;
array<Status> gDestStatus(17);

MidiEvent gEvent;   // reusable event object TODO, is this best way?

bool gFollowCC = false;
bool gFollowPB = false;
bool gFollowAT = false;
bool gFollowPP = false;
double gContinuation = 0;
bool STOPPED=true;

/*******************************************
 * UpdateInputParameters callback from GUI
 *******************************************/

void updateInputParameters() { 
   if(inputParameters[0] == 0) gFollowCC = false;
   else gFollowCC = true;

   if(inputParameters[1] == 0) gFollowPB = false;
   else gFollowPB = true;

   if(inputParameters[2] == 0) gFollowAT = false;
   else gFollowAT = true;

   if(inputParameters[3] == 0) gFollowPP = false;
   else gFollowPP = true;

   // continuation amount in samples converted from ms
   gContinuation = (inputParameters[4]/1000) * sampleRate;
} 


/*******************************************
 * Reset
 *******************************************/

// init the status structures
bool initialize() {
    resetStatus();
    return true;
}

// mainly for the sake of LogicPro, may not be needed?
void reset() {
    resetStatus();
}

/*******************************************
 * Reset the status structures
 *******************************************/

void resetStatus() {

    for(int i=0; i<128; i++) {
        gSrcStatus.cc[i] = 1000; // 1000 = undefined
        gSrcStatus.pp[i] = 1000;
    }
    gSrcStatus.ccnumlist.resize(0);
    gSrcStatus.ppnumlist.resize(0);
    gSrcStatus.at = 0;
    gSrcStatus.pb = 0;
    gSrcStatus.notes = 0;    // not used in src
    gSrcStatus.lastSample = 0; // not used in src

    // destStatus reset on 16 channels
    for(uint8 chan=1; chan<=16; chan++) {
        Status@ ds = @gDestStatus[chan];
        for(int i=0; i<128; i++) {
            ds.cc[i] = 1000; // 1000 = undefined
            ds.pp[i] = 1000; // 1000 = undefined
        }
        ds.ccnumlist.resize(0);
        ds.ppnumlist.resize(0);
        ds.at = 0;
        ds.pb = 0;
        ds.notes = 0;
        ds.lastSample = 0;
    }
}


/******************************************
 * processBlock
 ******************************************/

void processBlock(BlockData& data)
{
    /*****************************************
     * first check playing status, if recently
     * started, then perform reset Status
     *****************************************/

    if(data.transport.isPlaying) {
        if(STOPPED) {
            resetStatus();
            STOPPED=false;
        }
    }
    else {
        STOPPED=true;
    }

    /**************************************
     * process midi queue
     **************************************/

    for(uint i=0;i<data.inputMidiEvents.length;i++) {

        const MidiEvent@ event = data.inputMidiEvents[i];
        MidiEventType type = MidiEventUtils::getType(event);
 
        /*******************************************
         * pass PC through as-is
         *******************************************/

        if (type == kMidiProgramChange ) {
            // send event
            data.outputMidiEvents.push(event);
            continue;
        }

        /*****************
         * NoteOn
         *****************/

        if (type == kMidiNoteOn) {
            handleNoteOn(data, event);
            continue;
        }

        /*****************
         * NoteOff
         *****************/

        if (type == kMidiNoteOff) {
            handleNoteOff(data, event);
            continue;
        }

        /*********************************
         * CC, AT and PB Expression events
         *********************************/

        handleExpression(data, event);
    }
}


/*********************************************
 * Handle NoteOn events
 *********************************************/

void handleNoteOn(BlockData& data, const MidiEvent& event) {

    uint8 chan = MidiEventUtils::getChannel(event);
    Status@ ds = @gDestStatus[chan];
    ds.notes++;
    ds.lastSample = 0; // reset continuation time, zero means sustaining

    // Chase CC, PB and AT events for this NoteOn
    if (gFollowCC) chaseCC(data, event);
    if (gFollowPP) chasePP(data, event);
    if (gFollowPB) chasePB(data, event);
    if (gFollowAT) chaseAT(data, event);

    // send NoteOn Event
    data.outputMidiEvents.push(event);
}

/*********************************************
 * Handle NoteOff events
 *********************************************/

void handleNoteOff(BlockData& data, const MidiEvent& event) {

    uint8 chan = MidiEventUtils::getChannel(event);
    Status@ ds = @gDestStatus[chan];
    ds.notes--;
    if (ds.notes == 0) {
        ds.lastSample = data.transport.positionInSamples + event.timeStamp;
    }

    // send NoteOff event
    data.outputMidiEvents.push(event);
}


/*********************************************
 * Handle Expression events
 *********************************************/

void handleExpression(BlockData& data, const MidiEvent& event) {

    // Make sure following is enabled
    MidiEventType type = MidiEventUtils::getType(event);
    if ( ! isFollowEnabled(type)) {
        // send event
        data.outputMidiEvents.push(event);
        setSrcRunning(event); 
        return;
    }


    // forward to all needed channels
    followChannels(data, event);

    // updating the received running value
    setSrcRunning(event);
}

/*********************************************
 *
 * followChannels
 *
 * method will look at all
 * channels where events might be sustaining
 * and send the given decoration event to them
 * if so, otherwise, stash the value for future
 * chasing, only look through current port
 *********************************************/

void followChannels(BlockData& data, const MidiEvent& event) {

    // always send this expression even through to its current
    // channel.  With DP it has already been channelized, with
    // non-DP still needs to be channelized, which means it 
    // will be duplicated.  Might want to change this so that
    // in DP Mode, its sent here, but in non-DP mode we only 
    // send here when it never gets channelized below.

    data.outputMidiEvents.push(event);
    setDestRunning(event);

    uint8 evtchan = MidiEventUtils::getChannel(event);
    int64 position = data.transport.positionInSamples;
    double samples = position + event.timeStamp;
 
    // now in order to handle possibility of chord, Look for more
    // channels currently sustaining.

    for(uint8 chan=1; chan<=16; chan++) {

        if(chan==evtchan) continue;  // skip the one we already sent

        if(isSustaining(chan, samples)) {

            MidiEventType type = MidiEventUtils::getType(event);
            Status@ ds = @gDestStatus[chan];
            uint number, value;
            int pbval;

            switch(type) {
            case kMidiNoteAfterTouch:
                number = MidiEventUtils::getNote(event);
                value = MidiEventUtils::getNoteVelocity(event);
                if(value != ds.pp[number]) {
                    MidiEventUtils::setType(gEvent, kMidiNoteAfterTouch);
                    MidiEventUtils::setChannel(gEvent, chan);
                    MidiEventUtils::setNote(gEvent, number);
                    MidiEventUtils::setNoteVelocity(gEvent, value);
                    gEvent.timeStamp = event.timeStamp;
                    data.outputMidiEvents.push(gEvent);
                    setDestRunning(gEvent);
                }
                break;

            case kMidiControlChange:
                number = MidiEventUtils::getCCNumber(event);
                value = MidiEventUtils::getCCValue(event);
                if(value != ds.cc[number]) {
                    MidiEventUtils::setType(gEvent, kMidiControlChange);
                    MidiEventUtils::setChannel(gEvent, chan);
                    MidiEventUtils::setCCNumber(gEvent, number);
                    MidiEventUtils::setCCValue(gEvent, value);
                    gEvent.timeStamp = event.timeStamp;
                    data.outputMidiEvents.push(gEvent);
                    setDestRunning(gEvent);
                }
                break;

            case kMidiChannelAfterTouch:
                value = MidiEventUtils::getChannelAfterTouchValue(event);
                if(value != ds.at) {
                    MidiEventUtils::setType(gEvent, kMidiChannelAfterTouch);
                    MidiEventUtils::setChannel(gEvent, chan);
                    MidiEventUtils::setChannelAfterTouchValue(gEvent, value);
                    gEvent.timeStamp = event.timeStamp;
                    data.outputMidiEvents.push(gEvent);
                    setDestRunning(gEvent);
                }
                break;

            case kMidiPitchWheel:
                pbval = MidiEventUtils::getPitchWheelValue(event);
                if(pbval != ds.pb) {
                    MidiEventUtils::setType(gEvent, kMidiPitchWheel);
                    MidiEventUtils::setChannel(gEvent, chan);
                    MidiEventUtils::setPitchWheelValue(gEvent, pbval);
                    gEvent.timeStamp = event.timeStamp;
                    data.outputMidiEvents.push(gEvent);
                    setDestRunning(gEvent);
                }
                break;
            }
        } // end if sustaining
    } // end channel loop
}


/*********************************************
 * chasing methods handling chasing for each 
 * event type
 *********************************************/

void chaseCC(BlockData& data, const MidiEvent& event) {

    uint8 chan = MidiEventUtils::getChannel(event);
    Status@ ds = @gDestStatus[chan];
  
    for (uint c=0; c<gSrcStatus.ccnumlist.length; c++) {

        uint ccnum = gSrcStatus.ccnumlist[c];

        uint dv = ds.cc[ccnum];
        uint sv = gSrcStatus.cc[ccnum];

        if (sv != dv && sv < 128) {

            MidiEventUtils::setType(gEvent, kMidiControlChange);
            MidiEventUtils::setChannel(gEvent, chan);
            MidiEventUtils::setCCNumber(gEvent, ccnum);
            MidiEventUtils::setCCValue(gEvent, sv);
            gEvent.timeStamp = event.timeStamp;

            data.outputMidiEvents.push(gEvent);

            ds.cc[ccnum] = sv;
        }
    }
}


void chasePP(BlockData& data, const MidiEvent& event) {

    uint8 chan = MidiEventUtils::getChannel(event);
    Status@ ds = @gDestStatus[chan];
  
    for (uint p=0; p<gSrcStatus.ppnumlist.length; p++) {

        uint ppnum = gSrcStatus.ppnumlist[p];

        uint dv = ds.pp[ppnum];
        uint sv = gSrcStatus.pp[ppnum];

        if (sv != dv && sv < 128) {

            MidiEventUtils::setType(gEvent, kMidiNoteAfterTouch);
            MidiEventUtils::setChannel(gEvent, chan);
            MidiEventUtils::setNote(gEvent, ppnum);
            MidiEventUtils::setNoteVelocity(gEvent, sv);
            gEvent.timeStamp = event.timeStamp;

            data.outputMidiEvents.push(gEvent);

            ds.pp[ppnum] = sv;
        }
    }
}


void chaseAT(BlockData& data, const MidiEvent& event) {

    uint8 chan = MidiEventUtils::getChannel(event);
    Status@ ds = @gDestStatus[chan];
    
    if (gSrcStatus.at != ds.at) {

        MidiEventUtils::setType(gEvent, kMidiChannelAfterTouch);
        MidiEventUtils::setChannel(gEvent, chan);
        MidiEventUtils::setChannelAfterTouchValue(gEvent, gSrcStatus.at);
        gEvent.timeStamp = event.timeStamp;

        data.outputMidiEvents.push(gEvent);

        ds.at = gSrcStatus.at;
    }
}

void chasePB(BlockData& data, const MidiEvent& event) {

    uint8 chan = MidiEventUtils::getChannel(event);
    Status@ ds = @gDestStatus[chan];
    
    if (gSrcStatus.pb != ds.pb) {
        MidiEventUtils::setType(gEvent, kMidiPitchWheel);
        MidiEventUtils::setChannel(gEvent, chan);
        MidiEventUtils::setPitchWheelValue(gEvent, gSrcStatus.pb);
        gEvent.timeStamp = event.timeStamp;

        data.outputMidiEvents.push(gEvent);

        ds.pb = gSrcStatus.pb;
    }
}

/************************************************
 * isFollowEnabled per event type
 ************************************************/

bool isFollowEnabled(MidiEventType type) {

    switch(type) {
    case kMidiControlChange:
        return gFollowCC;

    case kMidiChannelAfterTouch:
        return gFollowAT;

    case kMidiNoteAfterTouch:
        return gFollowAT;

    case kMidiPitchWheel:
        return gFollowPB;
    }

    return false;
}


/*********************************************
 * return status of channel, whether any 
 * sustaining notes 
 * NOTE, this version has been hacked to support
 *       DP, see script comment at top
 *********************************************/

bool isSustaining(uint8 chan, double samplesNow) {
    
    Status@ ds = @gDestStatus[chan];

    // If notes are still sustaining
    if (ds.notes > 0) {
        return true;
    }
    // case of recent reset I guess
    if(ds.lastSample == 0) {
        return false;
    }
    // check for continuation
    if((samplesNow - ds.lastSample) < gContinuation) {
        return true;
    }
    else {
        return false;
    }
}


/************************************************
 * setSrcRunning function per event type
 ************************************************/

void setSrcRunning(const MidiEvent& event) {

    MidiEventType type = MidiEventUtils::getType(event);

    uint number, value;
    int pbval;

    switch(type) {
        case kMidiControlChange:
            number = MidiEventUtils::getCCNumber(event);
            value = MidiEventUtils::getCCValue(event);
            if(gSrcStatus.cc[number] > 127) {
                gSrcStatus.ccnumlist.insertLast(number);
            }
            gSrcStatus.cc[number] = value;
            break;
        case kMidiNoteAfterTouch:
            number = MidiEventUtils::getNote(event);
            value = MidiEventUtils::getNoteVelocity(event);
            if(gSrcStatus.pp[number] > 127) {
                gSrcStatus.ppnumlist.insertLast(number);
            }
            gSrcStatus.pp[number] = value;
            break;
        case kMidiChannelAfterTouch:
            value = MidiEventUtils::getChannelAfterTouchValue(event);
            gSrcStatus.at = value;
            break;
        case kMidiPitchWheel:
            pbval = MidiEventUtils::getPitchWheelValue(event);
            gSrcStatus.pb = pbval;
            break;
        default:
            return;
    }
}

/************************************************
 * setDestRunning method per event type
 ************************************************/

void setDestRunning(const MidiEvent& event) {

    MidiEventType type = MidiEventUtils::getType(event);
    uint8 chan = MidiEventUtils::getChannel(event);
    Status@ ds = @gDestStatus[chan];
    uint number, value;
    int pbval;

    switch(type) {

        case kMidiControlChange:
            number = MidiEventUtils::getCCNumber(event);
            value = MidiEventUtils::getCCValue(event);
            if(ds.cc[number] > 127) {
                ds.ccnumlist.insertLast(number);
            }
            ds.cc[number] = value;
            break;
        case kMidiNoteAfterTouch:
            number = MidiEventUtils::getNote(event);
            value = MidiEventUtils::getNoteVelocity(event);
            if(ds.pp[number] > 127) {
                ds.ppnumlist.insertLast(number);
            }
            ds.pp[number] = value;
            break;
        case kMidiChannelAfterTouch:
            value = MidiEventUtils::getChannelAfterTouchValue(event);
            ds.at = value;
            break;

        case kMidiPitchWheel:
            pbval = MidiEventUtils::getPitchWheelValue(event);
            ds.pb = pbval;
            break;
    }
}
