--
---  EventChaser
--
-- This script will forward CC, PitchBend and ChannelPressure aftertouch
-- messages to all channels between 1-127 where notes are currently 
-- active (and after NoteOff for a short continuation period).
--
-- @script      com.example.script
-- @type        DSP
-- @author      Steve Schow
-- 
-- Version 3.00
-- 
-- Any input msgs from any channel will be forwarded.
--
-- FUTURE, multi port handling?
--
------------------------------------------------------------------------

local MidiBuffer    = require ('el.MidiBuffer')
local script        = require ('el.script')
local round         = require ('el.round')

function layout()
    return {
        -- two ins, two outs
        audio = { 0, 0 },
        -- only need one MIDI in.
        midi  = { 1, 1 }
    }
end

-----------------------------------------------------
-- Some temporary global variables until I figure out
-- how to use parameters and UI stuff properly
-----------------------------------------------------
gFollowCC = true
gFollowPB = true
gFollowAT = true
gFollowPP = true
-- set to 100ms as ms / 1000 - sampleRate
gContinuation = 100 / 1000 - 48000

-------------------------------------------------------------
-- Some globals that will be used throughout script to avoid
-- having to pass around a million parameters
-------------------------------------------------------------
local gOutput = MidiBuffer.new()
local gTransport
local gParams
local STOPPED=true

-- setup global tables for src and dest status, dest has 16 channels
local gSrcStatus = {}
local gDestStatus = {}

-----------------------------------
-- prepare callback
-----------------------------------
function prepare(rate, block)
    gOutput:reserve(256)
    gOutput:clear()
    -- call the resetStatus when script loaded
    resetStatus()
end


-----------------------------------
-- process callback
-----------------------------------
function process(audioBuf,midiPipe,iParams,oParams,transport)

    -- first check playing status, if recently started, then
    -- reset status

    if(transport.playing()) then
        if(STOPPED) then
            resetStatus()
            STOPPED = false
        end
    else
        STOPPED=true
    end

    -- save a couple args to globals TODO is this ok?
    gTransport = transport
    gParams = iParams

    -- input and output midi buffers
    local input = midiPipe:get(1)
    gOutput:clear()

    -- loop through input buffer, handle each midi msg
    for msg,tstamp in input:messages() do

         -- pass PC through as-is
        if (msg.isProgram()) then
            gOutput:insert(msg, tstamp)

         -- NoteOn
        elseif (msg.isNoteOn()) then
            handleNoteOn(msg)

         -- NoteOff
        elseif (msg.isNoteOff()) then
            handleNoteOff(msg)

         -- CC, AT, PP and PB Expression msgs
        else
            handleExpression(msg)
        end
    end

    -- TODO why? Is this most efficient way?
    input:swap(gOutput)
end


-------------------------------------------------------------
-- resetStatus method called by Reset callback 
-- resets all running status
-------------------------------------------------------------
local function resetStatus()

    gSrcStatus = {}
    gSrcStatus.cc = {}
    gSrcStatus.pp = {}
    gSrcStatus.notes = 0
    gSrcStatus.lastSample = 0
    gSrcStatus.at = nil
    gSrcStatus.pb = nil

    gDestStatus = {}
    for chan = 1,16
    do
        gDestStatus[chan] = {}
        gDestStatus[chan].cc = {}
        gDestStatus[chan].pp = {}
        gDestStatus[chan].notes = 0
        gDestStatus[chan].lastSample = 0
        gDestStatus[chan].at = nil
        gDestStatus[chan].pb = nil
    end

end

--------------------------------------------------
-- handleNoteOn method handles incoming NoteOn msg
--------------------------------------------------

local function handleNoteOn(msg)

    local ds = gDeststatus[msg.channel()]
    ds.notes = ds.notes + 1
    ds.lastSample = 0

    -- chase CC, PB, AT and PP
    if(gFollowCC) then
        chaseCC(msg)
    end
    if(gFollowPP) then
        chasePP(msg)
    end
    if(gFollowPB) then
        chasePB(msg)
    end
    if(gFollowAT) then
        chaseAT(msg)
    end

    -- send actual NoteOn
    -- TODO, do we really need time specified here?
    gOutput:insert(msg,msg.time())
end

-----------------------------------------------------
-- handleNoteOff method handles incoming NoteOff msg
-----------------------------------------------------

local function handleNoteOff(msg)
    local chan = msg.channel()
    local ds = gDestStatus[chan]
    ds.notes = ds.notes - 1
    if(ds.notes == 0) then
        -- TODO is this correct?
        ds.lastSample = gTransport.frame() + msg.time()
    end

    -- send actual NoteOff
    -- TODO do we actually need time here?
    gOutput:insert(msg, msg.time())
end

-----------------------------
-- Handle Expression msgs
-----------------------------
local function handleExpression(msg)

    -- Make sure following is enabled
    if ( not isFollowEnabled(msg)) then
        -- send msg
        gOutput:insert(msg,msg.time())
        setStatus(gSrcStatus, msg) 
        return
    end

    -- forward to all needed channels
    followChannels(msg)

    -- updating the received running value
    setStatus(gSrcStatus, msg)
end

--------------------------------------------------
-- followChannels
--
-- method will look at all
-- channels where msgs might be sustaining
-- and send the given decoration msg to them
-- if so, otherwise, stash the value for future
-- chasing, only look through current port
--------------------------------------------------
local function followChannels(exprMsg)

    -- always send this expression even through to its current
    -- channel.  With DP it has already been channelized, with
    -- non-DP still needs to be channelized, which means it 
    -- will be duplicated.  Might want to change this so that
    -- in DP Mode, its sent here, but in non-DP mode we only 
    -- send here when it never gets channelized below.

    gOutput:insert(exprMsg, exprMsg.time())
    local chan = exprMsg.channel()
    local ds = gDestStatus[chan]
    setStatus(ds, exprMsg)

    -- TODO is this correct?
    local samples = gTransport.frame() + exprMsg.time()
 
    -- now in order to handle possibility of chord, Look for more
    -- channels currently sustaining.

    -- TODO look for opportunity to keep raw midi information in status
    --      so that we can quickly create new midi msgs.

    for chan = 1,16 
    do
        if(chan ~= exprMsg.channel() and
                isSustaining(chan, samples)) then

            forwardToChan(exprMsg,i chan)

        end
    end
end

------------------------------------------------------
-- forward a midi expression event to a given channel
-- but only if it is a different value then the current
-- running value on that channel
------------------------------------------------------
local function forwardToChan(exprMsg, chan)

    local ds = gDestStatus[chan]

    if(exprMsg.isController()) then
        local number = exprMsg.controller()
        if(exprMsg.controllerValue() ~= ds.cc[number]) then
           -- TODO change to avoid new
            local msg = MidiMessage.new(exprMsg.data())
            msg.setChannel(chan)
            gOutput:insert(msg, msg.time())
            setStatus(ds, msg)
        end


    elseif(exprMsg.isPitch()) then
        if(exprMsg.pitch ~= ds.pb) then
            -- TODO, can we avoid new
            local msg = MidiMessage.new(exprMsg.data())
            msg.setChannel(chan)
            gOutput:insert(msg, msg.time())
            setStatus(ds, msg)
        end

    elseif(exprMsg.isAfterTouch()) then
        if(exprMsg.afterTouch() ~= ds.at) then
            -- TODO can we avoid new
            local msg = MidiMessage.new(exprMsg.data())
            msg.setChannel(chan)
            gOutput:insert(msg, msg.time())
            setStatus(ds, msg)
        end

    -- TODO this is not functional PP
    elseif(exprMsg.isPressure()) then
        if(exprMsg.pressure() ~= ds.pp) then
            -- TODO can we avoid new
            local msg = MidiMessage.new(exprMsg.data())
            gOutput:insert(msg, msg.time())
            setStatus(ds, msg)
        end
    end
end


-------------------------------------------------------
-- Chase functions for each expression midi type
-------------------------------------------------------

local function chaseCC(msg)

    local chan = msg.channel()
    local ds = gDestStatus[chan]

    for num,sval in pairs(gSrcStatus.cc)
    do
        local dv = ds.cc[num]

        if(sval ~= nil and sval ~= dv) then
            local oMsg = MidiMessage:new()
            -- TODO, create CC event here
            gOutput:insert(oMsg, msg.time())
            ds.cc[num] = sval
        end
    end
end

local function chasePP(msg)

    local chan = msg.channel()
    local ds = gDestStatus[chan]

    for pitch,sval in pairs(gSrcStatus.cc)
    do
        local dv = ds.pp[pitch]

        if(sval ~= nil and sval ~= dv) then
            local oMsg = MidiMessage:new()
            -- TODO, create PP event here
            gOutput:insert(oMsg, msg.time())
            ds.pp[pitch] = sval
        end
    end
end

local function chaseAT(msg)

    local chan = msg.channel()
    local ds = gDestStatus[chan]

    if(gSrcStatus.at ~= nil and gSrcStatus.at ~= ds.at) then
        local oMsg = MidiMessage:new()
        -- TODO, create AT event here
        gOutput:insert(oMsg, msg.time())
        ds.at = gSrcStatus.at
    end
end

local function chasePB(msg)

    local chan = msg.channel()
    local ds = gDestStatus[chan]

    if(gSrcStatus.pb ~= nil and gSrcStatus.pb ~= ds.pb) then
        local oMsg = MidiMessage:new()
        -- TODO, create PB event here
        gOutput:insert(oMsg, msg.time())
        ds.pb = gSrcStatus.pb
    end
end


-----------------------------------------------
-- isSustaining method
-----------------------------------------------

local function isSustaining(chan, samplesNow)

    local ds = gDestStatus[chan]

    if ds.notes > 0 then
        return true
    end
    if ds.lastSample == 0 then
        return false
    end
    if ((samplesNow - ds.lastSample) < gContiuation) then
        return true
    else
        return false
    end
end

----------------------------------------------
-- isFollowEnabled, check UI per event type
----------------------------------------------
local function isFollowEnabled(msg)

    if msg.isController() then
        return gFollowCC
    elseif msg.isAfterTouch() then
        return gFollowAT
    elseif msg.isPressure() then
        return gFollowPP
    elseif msg.isPitch() then
        return gFollowPB
    else 
        return false
    end
end


--------------------------------------------------
-- Set the running status to the given status
-- object which could be the src status or could
-- be deststatus by channel.
--------------------------------------------------
local function setStatus(status, msg)

    if msg.isController() then
        local number = msg.controller()
        status.cc[number] = msg.controllerValue()

    elseif msg.isPressure() then
        local pitch = msg.pitch()
        status.pp[pitch] = msg.pressure()

    elseif msg.isAfterTouch() then
        status.at = msg.aftertouch()

    elseif msg.isPitch() then
        status.pb = msg.pitch()
    end
end

return {
    type        = 'DSP',
    layout      = layout,
    parameters  = parameters,
    process     = process,
    prepare     = prepare
}


--[[
    TODO

    - update comments to Kushview Lua standards, including for docs

    - get basic UI and params working properly

    -  get more sophisticated UI working 

    - sampleRate?

    - reset for logicPro?

    - POlyPressure needs to be fixed.

    - Need to figure out how to create midi objects

    - need to verify timestamping is correctly used

    - Need to find way to avoid malloc/new and find out bottom line about
      how midimessages are copied into buffers, by reference or by value
      and same with the midibuffer itself when leaving the process method

--]]
